import React, {FC} from 'react'
import styles from './NewTodoBlock.module.scss'
import {observer} from 'mobx-react-lite'
import TodoState from '../../store/TodoState'

export const NewTodoBlock: FC = observer(() => {
  return (
    <div className={styles.wrapper}>
      <input type="text" placeholder='TITLE' value={TodoState.formTodo.title}
             onChange={e => TodoState.changeFormNewTodo('title', e.currentTarget.value)}/>
      <input type="text" placeholder='DESCRIPTION' value={TodoState.formTodo.description}
             onChange={e => TodoState.changeFormNewTodo('description', e.currentTarget.value)}/>
      <select value={TodoState.formTodo.when}
              onChange={e => TodoState.changeFormNewTodo('when', e.currentTarget.value)}>
        <option value='today'>TODAY</option>
        <option value='tomorrow'>TOMORROW</option>
      </select>
      <button disabled={!TodoState.formTodo.title} onClick={TodoState.addTodo}>ADD</button>
    </div>
  )
})