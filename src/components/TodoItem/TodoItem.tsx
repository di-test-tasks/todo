import React, {FC} from 'react'
import styles from './TodoItem.module.scss'
import {observer} from 'mobx-react-lite'
import TodoState from '../../store/TodoState'

type TProps = {
  id: number
  title: string
  description?: string
  completed: boolean
}

export const TodoItem: FC<TProps> = observer(({id, title, description, completed}) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <span className={styles.title}>{title}</span>
        <div className={styles.buttons}>
          <button className={completed ? styles.checked : styles.notChecked}
                  onClick={() => TodoState.completedTodo(id)}><i className="fas fa-check"/></button>
          <button className={styles.deleted} onClick={() => TodoState.deleteTodo(id)}><i className="fas fa-times"/>
          </button>
        </div>
      </div>
      {description && <span className={styles.description}>{description}</span>}
    </div>
  )
})