import React, {FC} from 'react'
import styles from './Block.module.scss'

type TProps = {
  title: string
  content: JSX.Element
  background?: string
}

export const Block: FC<TProps> = ({title, background, content}) => {
  return (
    <div className={styles.wrapper} style={{background}}>
      <span className={styles.title}>{title}</span>
      {content}
    </div>
  )
}