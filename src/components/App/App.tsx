import React, {FC, useEffect} from 'react'
import styles from './App.module.scss'
import {observer} from 'mobx-react-lite'
import AppState from '../../store/AppState'
import TodoState from '../../store/TodoState'
import {Block} from '../Block/Block'
import {NewTodoBlock} from '../NewTodoBlock/NewTodoBlock'
import {TodosBlock} from '../TodosBlock/TodosBlock'

export const App: FC = observer(() => {
  useEffect(() => {
    TodoState.checkLocalStorage()
  }, [])

  if (!AppState.isReady) {
    return <></>
  }
  return (
    <div className={`container ${styles.wrapper}`}>
      <Block title='Add new one' content={<NewTodoBlock/>}/>
      <Block title='My today todo' content={<TodosBlock when='today'/>} background='#C4C4C4'/>
      <Block title='My tomorrow todo' content={<TodosBlock when='tomorrow'/>}/>
    </div>
  )
})