import React, {FC} from 'react'
import styles from './TodosBlock.module.scss'
import {observer} from 'mobx-react-lite'
import TodoState, {TWhen} from '../../store/TodoState'
import {TodoItem} from '../TodoItem/TodoItem'

type TProps = {
  when: TWhen
}

export const TodosBlock: FC<TProps> = observer(({when}) => {
  const todos = TodoState.getTodos(when).map(i => <TodoItem key={i.id} id={i.id} title={i.title}
                                                            description={i.description}
                                                            completed={i.completed}/>)
  return (
    <div className={styles.wrapper}>
      {todos.length ? todos : <span>Список пуст</span>}
      {when === 'today' && todos.length ?
        <button className={styles.setAllCompleted} onClick={TodoState.setAllCompleted}>SET ALL COMPLETE</button> : null}
    </div>
  )
})