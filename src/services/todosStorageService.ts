export const localStorageName = 'todos'

export const setTodosLocalStorage = (data: string) => {
  return localStorage.setItem(localStorageName, data)
}

export const getTodosLocalStorage = () => {
  const data = localStorage.getItem(localStorageName)
  if (data) {
    return JSON.parse(data)
  }
  return false
}
