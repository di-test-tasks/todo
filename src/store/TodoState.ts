import {makeAutoObservable} from 'mobx'
import AppState from './AppState'
import {getTodosLocalStorage, setTodosLocalStorage} from '../services/todosStorageService'

export type TWhen = 'today' | 'tomorrow'
type TForm = {
  title: string, description: string, when: TWhen
}
type TFieldsForm = 'title' | 'description' | 'when'
type TTodo = {
  id: number, title: string, description: string, completed: boolean, when: TWhen
}

class TodoState {
  constructor() {
    makeAutoObservable(this)
  }

  formTodo: TForm = {
    title: '',
    description: '',
    when: 'today'
  }

  todos: Array<TTodo> = []

  checkLocalStorage = () => {
    const todos = getTodosLocalStorage()
    if (todos) {
      this.todos = todos
    }
    return AppState.setReady()
  }

  changeFormNewTodo = (field: TFieldsForm, value: string) => {
    this.formTodo = {...this.formTodo, [field]: value}
  }

  addTodo = () => {
    this.todos.push({
      id: new Date().getTime(),
      title: this.formTodo.title,
      description: this.formTodo.description,
      completed: false,
      when: this.formTodo.when
    })
    this.updateLocalStorage()
    this.cleanForm()
  }

  cleanForm = () => {
    this.formTodo = {title: '', description: '', when: 'today'}
  }

  completedTodo = (id: number) => {
    this.todos = this.todos.map(i => {
      if (i.id === id) {
        return {...i, completed: !i.completed}
      }
      return i
    })
    this.updateLocalStorage()
  }

  setAllCompleted = () => {
    this.todos = this.todos.map(i => {
      if (i.when === 'today') {
        if (!i.completed) {
          return {...i, completed: true}
        }
        return i
      }
      return i
    })
    this.updateLocalStorage()
  }

  deleteTodo = (id: number) => {
    this.todos = this.todos.filter(i => i.id !== id)
    this.updateLocalStorage()
  }

  getTodos = (when: TWhen) => {
    return this.todos.filter(i => i.when === when)
  }

  updateLocalStorage = () => {
    setTodosLocalStorage(JSON.stringify(this.todos))
  }
}

export default new TodoState()