import {makeAutoObservable} from 'mobx'

class AppState {
  constructor() {
    makeAutoObservable(this)
  }

  isReady = false

  setReady = () => {
    return this.isReady = true
  }
}

export default new AppState()